﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace TrelloReceiverAPI.Controllers
{
    public class TrelloReceiverController : ApiController
    {
        [HttpGet]
        public string GetTrello(System.Web.HttpRequestBase Request)
        {
            try
            {
                string content = string.Empty;
                string messageid = string.Empty;
                string htmlString = string.Empty;
                string description = string.Empty;
                using (Stream receiveStream = Request.InputStream)
                using (StreamReader readStream = new StreamReader(receiveStream, Request.ContentEncoding))
                {
                    content = readStream.ReadToEnd();
                }
                dynamic json = JsonConvert.DeserializeObject(content);
                if (json != null)
                {
                    messageid = json.action.id;
                    description = json.action.data.card.desc;
                    MarkdownSharp.Markdown transformer = new MarkdownSharp.Markdown();
                    content = transformer.Transform(description);
                    StringBuilder htmlBuilder = new StringBuilder();
                    htmlBuilder.Append(content);
                    htmlString = htmlBuilder.ToString();
                    string path = HttpContext.Current.Server.MapPath(@"~\HTMLFiles");
                    string filename = "Html" + messageid + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                    string filepath = path + "\\" + filename;
                    using (FileStream fs = new FileStream(filepath.Replace(".txt", ".html"), FileMode.Create))
                    {
                        using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                        {
                            w.WriteLine(htmlString);
                        }
                    }
                    //write to log
                    path = HttpContext.Current.Server.MapPath(@"~\HookLogs");
                    filepath = path + "\\" + filename;
                    if (File.Exists(filepath))
                    {
                        using (StreamWriter writer = new StreamWriter(filepath, true))
                        {
                            writer.WriteLine("-------------------START-------------" + DateTime.Now);
                            writer.WriteLine("-------------------START-------------" + DateTime.Now);
                            writer.WriteLine("----------------JSON Response Start------------------");
                            writer.WriteLine("JSON Response: " + json);
                            writer.WriteLine("-------------------JSON Response End END-------------" + DateTime.Now);
                            writer.WriteLine("------------------- END-------------");
                        }
                    }
                    else
                    {
                        StreamWriter writer = File.CreateText(filepath);
                        writer.WriteLine("-------------------START-------------" + DateTime.Now);
                        writer.WriteLine("----------------JSON Response Start------------------");
                        writer.WriteLine("JSON Response: " + json);
                        writer.WriteLine("-------------------JSON Response End END-------------" + DateTime.Now);
                        writer.WriteLine("------------------- END-------------");
                        writer.Close();
                    }
                }
                return "value";
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}